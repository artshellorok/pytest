import os
import re
import sys

class Tests:
    def __init__(self, path, env):
        sys.path.append(path) # adds provided directory to list we can import from
        self.path = path
        self.set_modules()

        for module_name in self.modules: # for each set module...
            env[module_name] = __import__(module_name)  
    
    
    def set_modules(self):
        __module_file_regexp = "(.+)\.py(c?)$"
        result = set()
    
        # Looks for all python files in the directory (not recursively) and add their name to result:
        for entry in os.listdir(self.path):
            if os.path.isfile(os.path.join(self.path, entry)):
                regexp_result = re.search(__module_file_regexp, entry)
                if regexp_result: # is a module file name
                    result.add(regexp_result.groups()[0])
    
        self.modules = sorted(result)

