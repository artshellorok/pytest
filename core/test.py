import os
import subprocess
import time
import glob

class Test:
    def __init__(self, path, name):
        self.root = path
        self.name = name
        self.command = os.environ['COMMAND']
        if os.environ['BACKSLASH'] == "true":
            self.logs = path + '\\' + 'logs' + '\\'
            self.file = path + '\\' + os.environ['FILE']
        else:
            self.logs = path + '/' + 'logs' + '/'
            self.file = path + '/' + os.environ['FILE']

    def exec(self, stdin=None):
        if self.type == "static":
            self.tests = sorted(glob.glob(self.root + self.directory_test + self.mask_test))
            self.answers = sorted(glob.glob(self.root + self.directory_ans + self.mask_ans))
            for test, ans in zip(self.tests, self.answers):
                test_file = open(test, "r").read().rstrip()
                ans_file = open(ans, "r").read().rstrip()
                process = subprocess.Popen(
                [self.command, self.file], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                res = process.communicate(input=str.encode(test_file))[0].decode().rstrip()
                if res != ans_file:
                    test_file_name = test.split('/')
                    test_file_name = test_file_name[len(test_file_name)-1]

                    print("On test: " + test_file_name)
                    self.fail(test_file, res, ans_file)

        elif self.type == "dynamic":
            process = subprocess.Popen(
                [self.command, self.file], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            return process.communicate(input=str.encode(stdin))[0].decode()

    def fail(self, stdin, expout, out):
        print("Test type: " + self.name)
        print("Error! Expected " + expout + " got " + out)
        with open(self.logs + self.name + "_" + str(time.strftime('%Y_%m_%d-%H_%I_%S-') + str(time.time()).split('.')[1]) + '.txt', "a") as file:
            file.write("input:\n")
            file.write(stdin + "\n\n")
            file.write("expected:\n")
            file.write(expout + "\n\n")
            file.write("got:\n")
            file.write(out)
        exit()
