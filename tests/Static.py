from core import Test
import random


class Static(Test):

    type = "static"

    # path should begin with / and end with /
    # root is pytest location
    directory_test = "/cases/"
    directory_ans = "/cases/"

    mask_test = "??"
    mask_ans = "??.a"

    def start(self):
        self.exec()
