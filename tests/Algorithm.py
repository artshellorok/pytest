from core import Test
import random


class Algorithm(Test):

    type = "dynamic"

    def start(self):
        for i in range(100):
            a = random.randint(10, 100000000000)
            b = random.randint(1, 99999999999999)
            stdin = str(a) + ' ' + str(b)
            ans = str(a + b)
            res = self.exec(stdin=stdin).rstrip()
            if ans != res:
                self.fail(stdin, ans, res)
